const connection = require('../utilities/connection');

let mFlixDb = {}

mFlixDb.showMovies = async () => {
    let movieModel = await connection.getMoviesCollection();
    let movieCollection = await movieModel.find({});

    if(movieCollection.length) return movieCollection;
    else return null;
}

mFlixDb.findMovie = async (movieId) => {
    let movieModel = await connection.getMoviesCollection();
    let movieData = await movieModel.find({movieId: `${movieId}`});

    if(movieData.length) return movieData;
    else return null;
}

/* Checks if the request movie for booking exists or not */
mFlixDb.checkAvailibility = async (bookingObj) => {
    let movieModel = await connection.getMoviesCollection();
    let movieData = await movieModel.find({});

    let movieAvailable = movieData.find((movie, index) => {
        if(movie.movieId == bookingObj.movieId) return true;
    })
    
    if(movieAvailable) return movieAvailable;
    else return null;
}

/* Generates a new bookingId every time a new booking is done */
mFlixDb.generateBookingId = async () => {
    let userModel = await connection.getUsersCollection();
    let bookingIds = await userModel.distinct("bookings.bookingId");
    let max_booking_Id = Math.max(...bookingIds);
    if (max_booking_Id > 0) return max_booking_Id + 1;
    else return 2001;
}

/* Books the required number of tickets for an existing movie */
mFlixDb.bookTicket = async (userId, bookingObj) => {
    let userModel = await connection.getUsersCollection();
    let userData = await userModel.findOne({userId: `${userId}`});
    
    let movieModel = await connection.getMoviesCollection();
    let movieData = await movieModel.findOne({movieId: bookingObj.movieId});

    let newBookingId = await mFlixDb.generateBookingId();
    let bookingCost = bookingObj.noOfTickets * movieData.movieCostPerTicket;
    let newAvailability = movieData.availability - bookingObj.noOfTickets;
    console.log(`New movie availability: ${newAvailability}`);

    const newBookingObject = {
        movieName: movieData.movieName,
        movieId: movieData.movieId,
        bookingId: newBookingId,
        noOfTickets: bookingObj.noOfTickets,
        bookingCost: bookingCost
    };
    //update movie availability
    console.log(bookingObj.movieId);
    let updatedMovieData = await movieModel.updateOne({movieId: bookingObj.movieId}, {availability: newAvailability});
    console.log(updatedMovieData);

    //add booking object to user object
    let newBookingObj = await userModel.updateOne({userId: userId}, {bookings: [...userData.bookings, newBookingObject]});
    console.log(newBookingObj);
    console.log(`Tickets booked`);
    return newBookingObject;
}

/* Retrieves all the bookings made by an existing user */
mFlixDb.previousBookings = async (userId) => {
    let userModel = await connection.getUsersCollection();
    let userData = await userModel.findOne({userId: `${userId}`});

    if(userData) return userData.bookings;
    else return null;
}

module.exports = mFlixDb;