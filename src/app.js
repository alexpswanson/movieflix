const express = require('express');
const path = require('path');
const app = express();
const router = require('./routes/routing');
// const errorLogger = require('./utilities/errorLogger');
// const requestLogger = require('./utilities/requestLogger');
const errorLogger = require('./utilities/winstonError');
const requestLogger = require('./utilities/winstonRequest');
const bodyParser = require('body-parser');

//Winston Error Logging

var expressWinston = require('express-winston');
var winston = require('winston');

/* Import the required modules here */

/* Configure the required middleware functions in proper order */

app.use(bodyParser.json());

app.use(requestLogger);

app.use('/', router);

app.use(errorLogger);

app.listen(3000, () => {
    console.log("server listening in port 3000");
});